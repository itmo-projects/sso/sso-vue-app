# Vue3 App Example for SSO Project
### Университет ИТМО | ITMO University

#### Setting up the environment
Выставляем IP для Resource Server (BASE_URL) и Auth Server (KEYCLOAK_URL) </br>
We will refence it after
```shell
export KEYCLOAK_URL=<host:port keycloak> &&
export REALM=<realm name> &&
export CLIENT_ID=<client id> &&
export BASE_URL=<backend url>
```

Запуск локально через `npm run serve`
```shell
VUE_APP_KEYCLOAK_URL="$KEYCLOAK_URL" &&
VUE_APP_REALM="$REALM" &&
VUE_APP_CLIENT_ID="$CLIENT_ID" &&
VUE_APP_BASE_URL="$BASE_URL"
```

### Запуск в Docker Container

```shell
docker build \
 --build-arg KEYCLOAK_URL \
 --build-arg REALM \
 --build-arg CLIENT_ID \
 --build-arg BASE_URL \
 --tag sso-vue-app:latest .
```

Запуск контейнера на 8080 порту
```shell
docker run -d \
        --name sso-vue-app \
        -d \
        -p 8080:8080 sso-vue-app:latest
```

#### Пример запуска
```shell
export KEYCLOAK_URL=http://172.17.0.4:8180 &&
export REALM=sso-itmo &&
export CLIENT_ID=sso-itmo-client &&
export BASE_URL=http://172.17.0.5:9000
```

### Получить IP-адрес контейнера
`docker inspect sso-vue-app --format '{{.NetworkSettings.IPAddress}}'`