import { createRouter, createWebHistory } from "vue-router";
import Main from "@/views/MainPage.vue";

const routes = [
  {
    path: "/",
    name: "Main",
    component: Main,
    meta: {
      authenticated: true,
    },
  },
  {
    path: '/:pathMatch(.*)*',
    name: "Error",
    component: () => import("@/views/ErrorPage.vue"),
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
