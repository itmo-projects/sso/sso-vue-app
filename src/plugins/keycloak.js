import Keycloak from "keycloak-js";

// const keycloak = new Keycloak({
//   url: "http://localhost:8180",
//   realm: "sso-itmo",
//   clientId: "sso-itmo-client",
// });

const keycloak = new Keycloak({
  url: process.env.VUE_APP_KEYCLOAK_URL,
  realm: process.env.VUE_APP_REALM,
  clientId: process.env.VUE_APP_CLIENT_ID,
});

keycloak.onTokenExpired = function () {
  keycloak
    .updateToken(180)
    .then(function (refreshed) {
      if (refreshed) {
        console.log(keycloak.token, 'keycloak.onTokenExpired');
        
        alert("Token refreshed");
        // write any code you required here
      } else {
        alert("Token is still valid now");
      }
    })
    .catch(function () {
      alert("Failed to refresh the token, or the session has expired");
    });
};

export { keycloak };
