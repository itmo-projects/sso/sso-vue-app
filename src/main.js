import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import vuetify from "@/plugins/vuetify";
import interceptorsSetup from '@/api/interceptors.js';

import "@/assets/styles.css";

interceptorsSetup();

const app = createApp(App);

app.use(router);
app.use(vuetify);

app.mount("#app");
