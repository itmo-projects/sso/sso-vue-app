import axiosInstance from "./setup.js";
import { keycloak } from "@/plugins/keycloak.js";

export default function interceptorsSetup() {
  axiosInstance.interceptors.request.use(
    (config) => {
      const token = keycloak.token;
      console.log("from anxios");
      if (token) {
        config.headers.Authorization = `Bearer ${token}`;
      }
      return config;
    },
    (error) => {
      return Promise.reject(error);
    }
  );

  axiosInstance.interceptors.response.use(
    (response) => {
      return response;
    },
    async (error) => {
      const originalRequest = error.config;

      if (error.response.status === 401 && !originalRequest._retry) {
        originalRequest._retry = true;

        try {
          await keycloak.updateToken(180);
          console.log(keycloak.token, "keycloak.token axios");

          const token = keycloak.token;

          originalRequest.headers.Authorization = `Bearer ${token}`;

          console.log("token updated");

          return axiosInstance(originalRequest);
        } catch (err) {
          console.error(err);
        }
      }

      return Promise.reject(error);
    }
  );
}
